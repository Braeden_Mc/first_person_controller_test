﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Master : MonoBehaviour // a game master is a central hub that keeps track of all gameplay mechanics like score, speed, and health
{
    public static Game_Master instance;

    private int currentScore = 0;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        UIManager.instance.UpdateScore(currentScore);
    }

    public void AddScore(int score)
    {
        currentScore += score;
        UIManager.instance.UpdateScore(currentScore);
    }
}
