﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class KeyRing : MonoBehaviour
{
    [SerializeField]

    private List<KeyColour> collectedKeys = new List<KeyColour>(); // Creation of a list and defining what type of instance the list counts

    public bool PickupKey(KeyColour colour)
    {
        if(collectedKeys.Contains(colour) == false)
        {
            collectedKeys.Add(colour);
            return true;
        }
        return false;   // A return function that isn't meant to occur in gameplay, just to signal a faulty function
    }

    public bool CheckKeys(KeyColour colour)
    {
        foreach(KeyColour c in collectedKeys)   // starting a for each loop
        {
            if(c == colour)
            {
                return true;
            }
        }
        return false;
    }
}
