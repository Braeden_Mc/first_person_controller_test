﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving_Platform : MonoBehaviour
{
    public Transform[] points;
    public float speed = 2;

    private Transform currentTarget;
    private int index = 0;

    void Start()
    {
       if(points.Length > 0)
        {
            currentTarget = points[index];
        }
    }

    void Update()
    {
      if(currentTarget != null)
        {
            if(Vector3.Distance(transform.position, currentTarget.position) < 0.5f)
            {
                index++;
                if(index >= points.Length)
                {
                    index = 0;
                }
                currentTarget = points[index];
                // here
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.transform.SetParent(transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            other.transform.SetParent(null);
        }
    }
}
