﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Obj_Interaction : MonoBehaviour
{
    public abstract void Activate();
}