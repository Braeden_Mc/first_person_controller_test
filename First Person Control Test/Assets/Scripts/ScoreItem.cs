﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreItem : MonoBehaviour
{
    public int value = 5;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Game_Master.instance.AddScore(value);
            gameObject.SetActive(false);
            enabled = false;   // this line ensures that the same thing can't be done several times to grind score points
        }
    }
}
