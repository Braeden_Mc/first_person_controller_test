﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    public float distance = 2.5f;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Use") == true)
        {
            if(Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit hit, distance) == true) //If Raycast hits something
            {
                Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * distance, Color.green, 0.5f);
                // try to reference an interactable component on the object the raycast hit
                Debug.Log(hit.transform.name);
                Obj_Interaction obj = hit.transform.GetComponent<Obj_Interaction>();
                // if the raycast hit object is not an interactable object
                if (obj == null)
                {
                    // attempt to reference interaction component from the parent of the raycast-hit object
                    obj = hit.transform.GetComponentInParent<Obj_Interaction>();
                }
                    // if the ray does hit an interactable object
                    if (obj != null)
                {
                    // if the hit object can be interpreted as a door
                    if(obj is DoorInteraction door)
                    {
                        // if the door is locked
                        if (door.locked == true)
                        {
                            // if the keyring has the colour associated with the door
                            if (GetComponent<KeyRing>().CheckKeys(door.unlockColour) == true)
                            {
                                door.UnlockDoor(true);
                            }
                        }
                        else
                        {
                            door.Activate();
                        }
                    }
                    else
                    {
                        obj.Activate();
                    }
                }
            }
        }
    }
}
