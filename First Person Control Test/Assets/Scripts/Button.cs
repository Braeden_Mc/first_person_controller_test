﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : Obj_Interaction
{
    public Transform target;   //Something to do with the door

    private Animator anim;

    private void Awake()
    {
        anim = transform.parent.GetComponent<Animator>();
    }

    public override void Activate()
    {
        Debug.Log("Button activated");
        anim.SetTrigger("Button Pressed");
        if(target != null)
        {
            if(target.tag == "Door")
            {
                Animator targetAnim = target.GetComponent<Animator>();
                bool toggle = targetAnim.GetBool("Toggle Door");
                targetAnim.SetBool("Toggle Door", !toggle);
            }
        }
    }
}
