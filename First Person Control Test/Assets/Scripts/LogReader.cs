﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LogReader : MonoBehaviour
{
    public TextFileManager fileManager;

    private void Start()
    {
        fileManager.CreateFile(fileManager.logName);
        fileManager.ReadFileContents(fileManager.logName);
    }
}

[System.Serializable]
public class TextFileManager
{
    [Tooltip("Defines the name of the text file to be read/written.")]
    public string logName;
    public string[] logContents;

    public void CreateFile(string fileName)
    {
        string dirPath = Application.dataPath + "/Resources/" + fileName + ".txt";
        if(File.Exists(dirPath) == false)
        {
            Directory.CreateDirectory(Application.dataPath + "/Resources/");   // create the directory and name it 'Resources'
            File.WriteAllText(dirPath, fileName + '\n');   // '\n' makes a text document move down to its next line
        }
    }

    // the public string below returns the contents of the specified file as an array of string values amd also sets 'logContents' variable to the same array
    public string[] ReadFileContents(string fileName)   
    {
        string dirPath = Application.dataPath + "/Resources/" + fileName + ".txt";
        string[] textContents = new string[0];
        if(File.Exists(dirPath) == true)
        {
            textContents = File.ReadAllLines(dirPath);
        }
        logContents = textContents;
        return textContents;
    }

    public void AddKeyValuePair(string fileName, string key, string value)
    {
        ReadFileContents(fileName);
        string dirPath = Application.dataPath + "/Resources/" + fileName + ".txt";
        string content = key + "," + value;
        string timeStamp = System.DateTime.Now.ToString();

        if(File.Exists(dirPath) == true)
        {
            bool contentsFound = false;
            for(int i = 0; i < logContents.Length; i++)   // starting the index at zero 
            {
                if (logContents[i].Contains(key) == true)   // if the string contains the key
                {
                    logContents[i] = timeStamp + " - " + content;
                    contentsFound = true;
                    break;
                }
            }

            if(contentsFound == true)
            {
                File.WriteAllLines(dirPath, logContents);
            }
            else
            {
                File.AppendAllText(dirPath, timeStamp + " - " + content + '\n');   // adds on to the end of the file
            }
        }
    }

    public string LocateValueByKey(string key)
    {
        string value = "";
        ReadFileContents(logName);
        foreach(string s in logContents)
        {
            if(s.Contains(key) == true)
            {
                string[] splitString = s.Split(',');   // looks for commas and splits the string wherever one is found
                value = splitString[splitString.Length - 1];
                break;   // breaks the loop
            }
        }
        return value;
    }
}