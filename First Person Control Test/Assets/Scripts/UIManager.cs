﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Image HealthFG;
    public Text healthText;
    public Text ScoreText;
    public Text timerText;
    public InputField input;

    public static UIManager instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    // Update is called once per frame
    public void UpdateHealth(Health health)
    {
        HealthFG.fillAmount = health.CurrentHealth / health.maxHealth;
        if(health.CurrentHealth > 0)
        {
            healthText.text = "Health: " + health.CurrentHealth;
        }
        else
        {
            healthText.text = "Dead";
        }
    }
    public void UpdateScore(int score)
    {
        ScoreText.text = "Score: " + score;
    }

    public void UpdateTimer(float time)
    {
        timerText.text = time.ToString();
    }
}
