﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerTrigger : MonoBehaviour
{
    public TextFileManager fileManager;

    private float timer = -1;
    private bool counting = false;

    // Start is called before the first frame update
    void Start()
    {
        fileManager.CreateFile(fileManager.logName);
        fileManager.ReadFileContents(fileManager.logName);
        timer = 0;
        counting = true;
        UIManager.instance.input.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(counting == true)
        {
            timer += Time.deltaTime;
            UIManager.instance.UpdateTimer(timer);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && counting == true)
        {
            counting = false;
            UIManager.instance.input.gameObject.SetActive(true);
        }
    }

    public void SaveTimeToFile()
    {
        fileManager.AddKeyValuePair(fileManager.logName, UIManager.instance.input.text, timer.ToString());
        UIManager.instance.input.gameObject.SetActive(false);
    }
}
