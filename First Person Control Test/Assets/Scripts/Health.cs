﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;      //Allows use of UI data types and functionality

public class Health : MonoBehaviour
{
    public Image HealthFG; //Image variable holds reference to the health bar

    public Text healthText; //Text variable holds reference to text inside health bar

    public float maxHealth = 100;

    public float CurrentHealth { get; private set; } = 0;

    // Start is called before the first frame update
    void Start()
    {
        CurrentHealth = maxHealth;
        UIManager.instance.UpdateHealth(this);

        HealthFG.fillAmount = CurrentHealth / maxHealth;
        healthText.text = "Health: " + CurrentHealth;
    }

    public void TakeDamage(float damage)
    {
        CurrentHealth -= damage;
        Debug.Log(CurrentHealth);
        if(CurrentHealth > maxHealth)
        {
            CurrentHealth = maxHealth;   // updates the health UI
        }
        else if (CurrentHealth <= 0)
        {
            CurrentHealth = 0;
        }   // CurrentHealth = 0; this is our original code. It's not as efficient as the new stuff.
            // healthText.gameObject.SetActive(false); //Turns the game object for the text off
            // Debug.Log("You Died");
            // HealthFG.fillAmount = CurrentHealth / maxHealth; //Updates health bar value when damage is taken
            // healthText.text = "Health: " + CurrentHealth; //Updates health bar text with current value
        UIManager.instance.UpdateHealth(this);
    }
}
