﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteraction : Obj_Interaction   /// changed to obj interaction to link to its activate function
{

    public bool locked = false;
    public KeyColour unlockColour;

    public override void Activate()
    {
        if(locked == false)
        {
            // open door
            Animator targetAnim = GetComponent<Animator>();
            bool toggle = targetAnim.GetBool("Toggle Door");
            targetAnim.SetBool("Toggle Door", !toggle); 
        }
    }

    public void UnlockDoor(bool activate)
    {
        if(locked == true)   // if(locked == true && c == unlockColour) the double & allows multiple things to be checked in an if statement but it isn't needed here
        {
            locked = false;
            if(activate == true)
            {
                Activate();
            }
        }
    }
}
